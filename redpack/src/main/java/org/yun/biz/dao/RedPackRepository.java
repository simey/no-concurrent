package org.yun.biz.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.yun.biz.model.RedPack;



/**
 * @ProjectName: no-concurrent
 * @ClassName: RedPackRepository
 * @Description: RedPackDao
 * @Author: liyunfeng31
 * @Date: 2020/10/5 0:42
 */
public interface RedPackRepository extends JpaRepository<RedPack, Long> {

}