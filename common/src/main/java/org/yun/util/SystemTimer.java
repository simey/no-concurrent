package org.yun.util;

import java.time.Instant;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class SystemTimer {
       private final static ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
       private static final long tickUnit = Long.parseLong(System.getProperty("systimer.tick", "10"));
       private static volatile long time = Instant.now().getEpochSecond();

       private static class TimerTicker implements Runnable {
           @Override
           public void run() { time = Instant.now().getEpochSecond(); }
      }

      public static long currentTime() { return time; }

      static {
            executor.scheduleAtFixedRate(new TimerTicker(), tickUnit, tickUnit, TimeUnit.MILLISECONDS);
             Runtime.getRuntime().addShutdownHook(new Thread() {
                 @Override
              public void run() {
             executor.shutdown();
              }}); }
} 
