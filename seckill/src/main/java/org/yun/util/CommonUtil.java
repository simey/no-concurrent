package org.yun.util;

import org.springframework.util.CollectionUtils;
import org.yun.constants.Constant;
import org.yun.exception.BizException;
import org.yun.exception.ErrorCode;

import java.util.Map;

/**
 * @author liyunfeng31
 */
public class CommonUtil {

    public static boolean checkActivity(Map<Object, Object> activity, boolean warn){
        if(CollectionUtils.isEmpty(activity)){
            throw new BizException(ErrorCode.SEC_KILL.NO_EXIST);
        }
        int state = Integer.parseInt(activity.get("state").toString());
        if(Constant.ONLINE != state){
            throw new BizException(ErrorCode.SEC_KILL.NO_ONLINE);
        }
        long st = Long.parseLong(activity.get("startTime").toString());
        long et = Long.parseLong(activity.get("endTime").toString());
        long now = SystemTimer.currentTime();
        if(et < now){
            throw new BizException(ErrorCode.SEC_KILL.OVER);
        }
        if(st > now){
            if(warn){ return false; }
            throw new BizException(ErrorCode.SEC_KILL.NO_STARTED);
        }
        return true;
    }

}
