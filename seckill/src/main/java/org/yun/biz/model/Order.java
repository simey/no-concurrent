package org.yun.biz.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.yun.util.IDUtil;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @ProjectName: no-concurrent
 * @ClassName: SkuStock
 * @Description: TODO(一句话描述该类的功能)
 * @Author: liyunfeng31
 * @Date: 2020/10/5 0:09
 */

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "seckill_order")
public class Order implements Serializable {

    @Id
    @Column(name = "order_id")
    private Long orderId;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "sku_id")
    private Long skuId;

    @Column(name = "activity_id")
    private Long activityId;

    @Column(name = "order_state")
    private Integer orderState;

    @Column(name = "pay_state")
    private Integer payState;

    @Column(name = "order_amount")
    private BigDecimal orderAmount;

    @Column(name = "uuid")
    private String uuid;

    @Column(name = "c_t")
    private Date ct;


    public static Order initOrder(Long userId, Long skuId, Long activityId, BigDecimal amount){

        return Order.builder()
                .orderId(IDUtil.id())
                .userId(userId)
                .skuId(skuId)
                .activityId(activityId)
                .orderAmount(amount)
                .orderState(0)
                .payState(0)
                .uuid(String.valueOf(userId) + skuId + activityId)
                .build();
    }
}
