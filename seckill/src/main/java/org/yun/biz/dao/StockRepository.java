package org.yun.biz.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import org.yun.biz.model.Stock;

import java.util.Date;


/**
 * @ProjectName: no-concurrent
 * @ClassName: StockRepository
 * @Description: 库存dao
 * @Author: liyunfeng31
 * @Date: 2020/10/5 0:42
 */
public interface StockRepository extends JpaRepository<Stock, Long> {

    /**
     * 扣库存防超卖 @Transactional(timeout = 10)
     * @param skuId skuId
     * @param decrement  skuNum
     * @return row
     */
    @Modifying(clearAutomatically = true)
    @Query(value = "update seckill_stock set valid_stock = valid_stock - ?2 where sku_id = ?1 and valid_stock - ?2 >= 0",nativeQuery = true)
    int decrStock(Long skuId, Integer decrement);

    /**
     * 根据shuId查询库存
     * @param skuId skuId
     * @return Stock
     */
    Stock findBySkuId(Long skuId);
}