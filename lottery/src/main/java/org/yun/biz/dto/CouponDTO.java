package org.yun.biz.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @ProjectName: no-concurrent
 * @ClassName: coupon
 * @Description: TODO(一句话描述该类的功能)
 * @Author: liyunfeng31
 * @Date: 2020/10/8 1:01
 */
@Data
public class CouponDTO implements Serializable {
    
    private String code;
    
    private String pwd;
}
