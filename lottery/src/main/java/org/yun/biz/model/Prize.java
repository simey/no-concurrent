package org.yun.biz.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @ProjectName: no-concurrent
 * @ClassName: Prize
 * @Description: 奖品-应该和奖项分开的 这里简化了
 * @Author: liyunfeng31
 * @Date: 2020/10/7 0:46
 */
@Data
@Entity
@Table(name = "prize")
public class Prize implements Serializable {

    /**
     * 奖品ID
     */
    @Id
    @Column(name = "prize_id")
    private Long prizeId;


    /**
     * 奖品名称
     */
    @Column(name = "prize_name")
    private String prizeName;


    /**
     * 抽奖活动ID
     */
    @Column(name = "lottery_id")
    private Long lotteryId;

    /**
     * 奖品类型 谢谢参与-1    普通奖品 1   具有唯一性的奖品2
     */
    @Column(name = "prize_type")
    private Integer prizeType;

    /**
     * 图片
     */
    @Column(name = "images")
    private String images;

    /**
     * 位置
     */
    @Column(name = "sort")
    private Integer sort;

    /**
     * 中奖率
     */
    @Column(name = "ratio")
    private BigDecimal ratio;

    /**
     * 总库存
     */
    @Column(name = "total_stock")
    private Integer totalStock;

    /**
     * 可用库存
     */
    @Column(name = "valid_stock")
    private Integer validStock;

    /**
     * 明细状态
     */
    @Column(name = "state")
    private Integer state;

    /**
     * 备注
     */
    @Column(name = "remark")
    private String remark;


    /**
     * 有效兑换开始时间
     */
    @Column(name = "exchange_st")
    private Integer exchangeSt;

    /**
     * 有效兑换结束时间
     */
    @Column(name = "exchange_et")
    private Integer exchangeEt;

    /**
     * 生效时间
     */
    @Column(name = "effective_time")
    private Integer effectiveTime;

    /**
     * 过期失效时间
     */
    @Column(name = "expiry_time")
    private Integer expiryTime;


    @Column(name = "c_t")
    private Date ct;

    @Column(name = "u_t")
    private Date ut;

    @Transient
    private String code;

    @Transient
    private String pwd;

}
