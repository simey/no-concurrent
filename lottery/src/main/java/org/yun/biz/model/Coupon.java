package org.yun.biz.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @ProjectName: no-concurrent
 * @ClassName: coupon
 * @Author: liyunfeng31
 * @Date: 2020/10/8 1:01
 */
@Data
@Entity
@Table(name = "coupon")
public class Coupon implements Serializable {

    @Id
    @Column(name = "coupon_id")
    private Long couponId;

    @Column(name = "code")
    private String code;

    @Column(name = "pwd")
    private String pwd;

    /**
     *状态：0默认 1缓存内 2使用过
     */
    @Column(name = "state")
    private Integer state;

    @Column(name = "prize_id")
    private Long prizeId;

    @Column(name = "c_t")
    private Date ct;

    @Column(name = "u_t")
    private Date ut;
}
