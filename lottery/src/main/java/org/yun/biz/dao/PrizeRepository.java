package org.yun.biz.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.yun.biz.model.Prize;


/**
 * @ProjectName: no-concurrent
 * @ClassName: PrizeRepository
 * @Description: 奖品
 * @Author: liyunfeng31
 */
public interface PrizeRepository extends JpaRepository<Prize, Long> {

}