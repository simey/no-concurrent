package org.yun;

import com.alibaba.fastjson.JSON;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.yun.biz.dao.CouponRepository;
import org.yun.biz.dao.PrizeRepository;
import org.yun.biz.dto.PrizeDTO;
import org.yun.biz.model.Coupon;
import org.yun.biz.model.Lottery;
import org.yun.biz.model.Prize;
import org.yun.biz.service.PrizeService;
import org.yun.lottery.LotteryService;
import org.yun.util.ArithmeticUtil;
import org.yun.util.RedisUtil;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.yun.constants.RedisConstant.COUPON_LIST;
import static org.yun.constants.RedisConstant.COUPON_REPLENISH_LIST;
import static org.yun.util.RedisUtil.COUPON_SCRIPT;

@SpringBootTest
class LotteryTests {

    @Resource
    private LotteryService lotteryService;

    @Resource
    private PrizeService prizeService;

    @Resource
    private PrizeRepository prizeDao;

    @Resource
    private RedisUtil redisUtil;

    @Resource
    private RedisTemplate redisTemplate;

    private static final Long userId = 101L;

    private static final Long userId2 = 202L;

    private static final Long activityId = 901L;

    private static final int num = 5;

    private static final int num2 = 15;

    private static final long time = 0;



    @Test
    void testApplyChance() {
        Boolean result1 = lotteryService.applyChance(userId,activityId,num,time);
        System.out.println(result1);
        Boolean result = lotteryService.applyChance(userId2,activityId,num2,time);
        System.out.println(result);
    }


    @Test
    void syncStockToCache() {
        prizeService.syncStockToCache();
    }



    @Test
    void testLottery() {

        Lottery lottery = new Lottery();
        lottery.setLotteryId(901L);
        lottery.setState(1);
        lottery.setStartTime(1602259200);
        lottery.setEndTime(1665331200);
        lottery.setTopic("这是主题哦");
        lottery.setPrizes(prizeDao.findAll());
     //   System.out.println(JSON.toJSONString(lottery));

        for (int i = 0; i < 50 ; i++) {
            PrizeDTO result = lotteryService.luckLottery(userId, activityId);
            System.out.println(result.toString());
        }
        System.out.println("===============================================");

    }

}
